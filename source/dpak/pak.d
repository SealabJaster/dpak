﻿module dpak.pak;

private
{
    import std.stdio        : File, SEEK_END, SEEK_SET;
    import std.bitmanip     : bigEndianToNative, nativeToBigEndian;
    import std.traits       : isArray, fullyQualifiedName, isScalarType, isSomeString;
    import std.range        : isOutputRange, ElementType;
    import std.file         : exists, remove;
    import std.algorithm    : find, canFind, filter;

    import std.exception;
}

/++
 + Describes an entry in a DPak file.
 ++/
struct DPakEntry
{
    /// Name of the entry
    string  name;

    /// Type of the entry
    string  type;

    /// Size of the entry's data in bytes.
    ulong   size;

    /// The position to the start of the entry's data in the DPak file.
    ulong   dataPointer;
}

class DPak
{
    private
    {
        const static char[4]    _magic      = "DPAK";
        const static ubyte      _version    = 0x01;

        File        _file;
        bool        _canUseFile;
        DPakEntry[] _entries;

        /++
         + Writes out header data.
         ++/
        void _writeHeader()
        {
            this._file.rawWrite(this._magic);
            this._file.rawWrite((&this._version)[0..1]);
        }

        /++
         + Writes out a length-prefixed string.
         + 
         + Parameters:
         +  data = The data to write.
         ++/
        void _writeLPString(string data)
        {
            enforce(data.length <= ubyte.max, 
                new DPakException("Attempted to write an LPString with a length larger than a ubyte"));

            auto size = cast(ubyte)data.length;
            this._file.rawWrite((&size)[0..1]);
            this._file.rawWrite(data);
        }

        /++
         + Reads and returns a length-prefixed string.
         + ++/
        string _readLPString()
        {
            ubyte size;
            enforce(this._file.rawRead((&size)[0..1]).length == 1, new DPakException("Unable to read size of string."));

            char[] buffer;
            buffer.length = size;
            enforce(this._file.rawRead(buffer).length == size), new DPakException("Unable to read in entire string.");

            return buffer.assumeUnique;
        }

        void _addEntry(string name, string type, ulong size, ulong pointer)
        {
            this._entries ~= DPakEntry(name, type, size, pointer);
        }

        /++
         + Discovers all entries within the DPak file.
         ++/
        void _discoverEntries()
        {
            ulong fileSize = 0;

            // Find the file size
            ulong temp = this._file.tell;
            this._file.seek(0, SEEK_END);
            fileSize = this._file.tell;
            this._file.seek(temp, SEEK_SET);

            while(this._file.tell < fileSize)
            {
                this._entries ~= this._readEntry();
            }
        }

        /++
         + Reads and returns a DPakEntry's metadata
         ++/
        DPakEntry _readEntry()
        {
            DPakEntry entry;
            entry.name = this._readLPString();
            entry.type = this._readLPString();

            enforce(this._file.rawRead((&entry.size)[0..1]).length == 1, new DPakException("Unable to read size of entry."));
            entry.dataPointer = this._file.tell;

            return entry;
        }

        /++
         + Reads in the file's header, and makes sure it's correct.
         ++/
        void _readHeader()
        {
            // TODO: Store the version the file was made with, and DPakException.
            ubyte[5] buffer;

            enforce(this._file.rawRead(buffer).length == buffer.length, new DPakException("Invalid DPak file. Bad header"));
            enforce(buffer[0..4] == this._magic, new DPakException("Invalid Dpak file. Bad header - Invalid magic number."));
            enforce(buffer[4] <= this._version, new DPakException("Invalid Dpak file. Incompatible versions."));
        }
    }

    public
    {
        /++
         + Opens or creates a new DPak file.
         + 
         + Parameters:
         +  filePath = The path to the file to create/open.
         ++/
        this(string filePath)
        {
            if(exists(filePath))
            {
                this._file.open(filePath, "rb+");
                this._readHeader();
                this._discoverEntries();
            }
            else
            {
                this._file.open(filePath, "wb+");
                this._writeHeader();
            }
        }

        /++
         + Writes a new entry into the DPak file.
         + 
         + Parameters:
         +  [Template]R = The Output range that will handle writing the data to the file.
         +  name        = The name of the entry.
         +  data        = The data to write.
         + 
         + Throws:
         +  DPakException if an entry with the name of $(B name) already exists.
         +  Any exception std.stdio.File may throw.
         ++/
        void writeEntry(R, T)(string name, T data, R range)
        if(canPackage!(R, T))
        {
            this._canUseFile = true;
            scope(exit) this._canUseFile = false;

            enforce(!this.entryExistsByName(name), new DPakException("Entry with the name of '"~name~"' already exists."));

            // Write metadata, and make sure we're at the end of the file first
            ulong size   = 0;
            ulong start  = 0;

            this.file.seek(0, SEEK_END);
            this._writeLPString(name);
            this._writeLPString(range.type);
            this.file.rawWrite((&size)[0..1]); // Pre-allocate the size bytes

            // Write the data out, and calculate the size
            start = this.file.tell;
            range.put!T(data);
            this._file.seek(0, SEEK_END);
            size = (this.file.tell - start);

            // Go back to the size bytes, and fill them in
            this.file.seek(start - size.sizeof, SEEK_SET);
            this.file.rawWrite(nativeToBigEndian(size));
            this.file.seek(0, SEEK_END);

            // Add in an entry
            this._addEntry(name, range.type, size, start);
        }

        /// Ditto
        void writeEntry(R, T)(string name, T data)
        if(canPackage!(R, T))
        {
            this.writeEntry(name, data, R(this));
        }

        /++
         + Reads an entry from the DPak file that has the specified name.
         + 
         + Parameters:
         +  [Template]R = The struct that can read in the entry.
         +  name        = The name of the entry to read in.
         + 
         + Returns:
         +  Whatever R.get!T returns.
         ++/
        T readEntry(R, T)(string name)
        {
            this._canUseFile = true;
            scope(exit) this._canUseFile = false;

            // Make sure the entry exists, and then seek to the start of it's data.
            auto aggregate = this.entries;
            auto finder    = aggregate.find!(ent => (ent.name == name));
            enforce(finder.length != 0, new DPakException("No entry with the name of '"~name~"' was found."));

            this.file.seek(finder[0].dataPointer);

            return R(this).get!T();
        }

        /++
         + Determines if an entry that has the specified name exists.
         + 
         + Parameters:
         +  name = The name of the entry to find.
         + 
         + Returns:
         +  True if the entry exists, false otherwise.
         ++/
        bool entryExistsByName(string name)
        {
            return this.entries.canFind!(ent => (ent.name == name));
        }

        /++
         + Convinience method for std.algorithm.filter to filter by type.
         + 
         + Paramaeters:
         +  type = The type to filter by
         ++/
        auto entriesByType(string type)
        {
            return this.entries.filter!(ent => (ent.type == type));
        }

        /++
         + Get the handle to the DPak's file.
         + Will throw an exception if used before a writeEntry or readEntry
         ++/
        @property
        ref File file()
        {
            enforce(this._canUseFile, new DPakException("The DPak's file handle can only be accessed while writing or reading."));

            return this._file;
        }

        /++
         + Get all entries that are part of this DPak file.
         ++/
        @property
        DPakEntry[] entries()
        {
            return this._entries;
        }
    }
}
unittest
{
    import std.stdio, std.format;

    string file = "Test.dat";
    if(exists(file))
    {
        std.file.remove(file);
    }
    auto pak = new DPak(file);

    // ScalarPacker
    ushort number = 52;
    pak.writeEntry!ScalarPacker("SomeNumber", number);
    assertThrown!DPakException(pak.writeEntry!ScalarPacker("SomeNumber", 0));

    auto got   = pak.readEntry!(ScalarPacker, ushort)("SomeNumber");
    assert(got == number, format("Got = 0x%X(%s)", got, got));

    // StringPacker
    string message = "Daniel is the soupiest of worlds.";
    pak.writeEntry!StringPacker("Message", message.dup); // .dup, because it turns into a char[] then.

    auto gotString = pak.readEntry!(StringPacker, char[])("Message");
    assert(gotString == message, gotString);

    // StringPacker(Array)
    string[] messages = ["Daniel", "is", "souper", "super"];
    pak.writeEntry!StringPacker("Messages", messages);

    auto gotMessages = pak.readEntry!(StringPacker, string[])("Messages");
    assert(gotMessages == messages, format("%s", gotMessages));

    // General
    assert(pak.entriesByType(ScalarPacker.type).front.name == "SomeNumber");
}

/++
 + Exception that is thrown when something goes wrong with a DPak file.
 ++/
class DPakException : Exception
{
    public
    {
        @nogc @safe pure nothrow this(string msg, string file = __FILE__, size_t line = __LINE__, Throwable next = null)
        {
            super(msg, file, line, next);
        }
    }
}

/++
 + Determines if $(B R) is able to package $(B T) into a DPak file.
 ++/
bool canPackage(R, T)()
{
    // TODO: Finish this function, and make sure that R has the "type" field.
    return isOutputRange!(R, T);
}

/++
 + Contains the ability to read and write Scalar types to a Dpak.
 ++/
struct ScalarPacker
{
    static assert(canPackage!(ScalarPacker, uint));  // Uint is just a random type I picked.

    private
    {
        DPak    _pak;
    }

    public
    {
        this(DPak pak)
        {
            this._pak   = pak;
        }

        /++
         + Writes the scalar out into the DPak file in big-endian.
         + 
         + Parameters:
         +  scalar = The thing to write.
         ++/
        void put(S)(S scalar)
        if(isScalarType!S && !isArray!S)
        {
            // Convert to big endian, and write it out
            this._pak.file.rawWrite(nativeToBigEndian(scalar));
        }

        /++
         + Reads a scalar from the DPak file from big-endian to the system's native endian.
         ++/
        S get(S)()
        if(isScalarType!S && !isArray!S)
        {
            ubyte[S.sizeof] buffer;
            auto read = this._pak.file.rawRead(buffer);

            // TODO: better message.
            enforce(read.length == buffer.length, new DPakException("Did not read in all bytes."));

            return bigEndianToNative!S(buffer);
        }

        /++
         + The string that identifies any entry originally written by this packer.
         ++/
        @property
        static string type()
        {
            return "dpak_scalar";
        }
    }
}

/++
 + Contains the ability to read and write string types to a Dpak.
 + Supports both the Char and String versions of strings for both reading and writing(wchar[], dstring, etc.)
 + Also supports Arrays of strings(char[][], dstring[], etc.)
 ++/
struct StringPacker
{
    static assert(canPackage!(StringPacker, wstring));
    static assert(canPackage!(StringPacker, dstring[]));
    
    private
    {
        DPak    _pak;
    }
    
    public
    {
        this(DPak pak)
        {
            this._pak = pak;
        }
        
        /++
         + Writes a length-prefixed string to the DPak file.
         + Maximum size of a string is uint.max
         + 
         + Parameters:
         +  data = The data to write.
         + ++/
        void put(S)(in S data)
        if(isSomeString!(S))
        {
            enforce(data.length <= uint.max, new DPakException("Given string's length exceeds uint.max"));
            ScalarPacker(this._pak).put!uint(data.length);
            
            this._pak.file.rawWrite(data);
        }
        
        /++
         + Writes an array of strings into the DPak entry.
         + 
         + Parameters:  
         +  data = The data to write.
         ++/
        void put(S)(in S data)
        if(isArray!S && isSomeString!(ElementType!(S)))
        {
            ScalarPacker(this._pak).put!uint(data.length);
            foreach(str; data)
            {
                this.put(str);
            }
        }
        
        /++
         + Reads in a length-prefixed string from the DPak file.
         + ++/
        S get(S)()
        if(isSomeString!(S))
        {
            auto size = ScalarPacker(this._pak).get!uint();
            
            charType!(S)[] buffer;
            buffer.length = size;
            
            enforce(this._pak.file.rawRead(buffer).length == size, new DPakException("Unable to read in string."));

            // If, for example, S = string, then we need to make it immutable.
            static if(!isImmutableString!S)
            {
                return buffer;
            }
            else
            {
                return buffer.assumeUnique;
            }
        }
        
        /++
         + Reads in an array of strings from the DPak entry.
         ++/
        S get(S)()
        if(isArray!S && isSomeString!(ElementType!(S)))
        {
            auto amount = ScalarPacker(this._pak).get!uint();
            
            S buffer;
            buffer.length = amount;
            
            foreach(i; 0..amount)
            {
                auto data = this.get!(charType!(ElementType!S)[]);

                // Make the data immutable, if it's something like a string instead of char[]
                static if(!isImmutableString!(ElementType!S))
                {
                    buffer[i] = data;
                }
                else
                {
                    buffer[i] = data.assumeUnique;
                }
            }
            
            return buffer;
        }
        
        @property
        static string type()
        {
            return "dpak_string";
        }
    }
}

private bool isImmutableString(S)()
{
    return (is(S == string) || is(S == dstring) || is(S == wstring));
}

private template charType(S)
{
    static if(is(S == string) || is(S == char[]))
    {
        alias charType = char;
    }
    else static if(is(S == wstring) || is(S == wchar[]))
    {
        alias charType = wchar;
    }
    else static if(is(S == dstring) || is(S == dchar[]))
    {
        alias charType = dchar;
    }
    else
    {
        static assert(false, "S isn't a string or char array type.");
    }
}